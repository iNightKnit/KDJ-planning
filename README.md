# KDJ-Planning
## Documenting much of the planning work needed for the creation of KDJ.

### What is KDJ?
KDJ is the LuaLaTeX document class portion of the not yet available Knit Design Journal design process documentation system.

#### What is Knit Design Journal?
An expandable and  version of the many knitting journals currently available - targeted at those knitters who design and knit their own lace projects, although documentation of most non-lace projects is possible as well.

### Creating the class
Knit Design Journal's intended function is somewhere between a professional portfolio and the documentation portion of a Capstone project, with additional physical deliverables. As long as the knitter continues to design, it is never complete. As such, there are considerations beyond those normally involved in creating a new TeX and friends class. This makes the class a multi-disciplinary effort, and so documentation of the planning stage is necessary in order not to leave anything out.

### ToDo
- [x] Determine contents of a Journal, including the most basic ordering
- [x] Choose the most appropriate baseclass based on chosen ordering and ideal basic document appearance
- [ ] Create a custom font to facilitate charting of lace patterns
- [ ] Create a LaTeX mockup using chosen baseclass to serve as a MWE and illustrate intended document structure when questions arise
- [ ] Determine which - if any - custom environments, commands and macros are necessary
- [ ] Investigate the titlepage environment
- [ ] Investigate multi-file documents
